﻿Namespace Entities
    Public Class Prospect
        Public Property Id As Guid
        Public Property Name As Name
        Public Property HomePhone As String
        Public Property CellPhone As String
        Public Overridable Property Address As Address
        Public Property AddressId As Guid
        Public Property Email As String
        Public Property Age As Int16
        Public Property Grade As String
        Public Property Gender As Gender
        Public Property Background As String
        Public Property FamilyNotes As String
        Public Property InActive As Boolean
        Public Property DateAdded As DateTime
        Public Property DateModified As DateTime
        Public Property DateDeactivated As DateTime
        Public Property OwnerId As Integer
    End Class

    Public Enum Gender
        NotSpecified
        Male
        Female
    End Enum

    Public Class Address
        Public Property Id As Guid
        Public Property StreetName As String
        Public Property HouseNumber As String
        Public Property Apt As String
        Public Property City As String
        Public Property State As String
        Public Property Zip As String
    End Class

    Public Class Name
        Public Property FirstName As String
        Public Property LastName As String
    End Class
End Namespace