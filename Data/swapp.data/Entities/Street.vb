﻿Namespace Entities
    Public Class Street
        Public Property Id As Guid
        Public Property Name As String
        Public Overridable Property Houses As ICollection(Of House)
        Public Property City As String
    End Class
End Namespace