﻿Namespace Entities
    Public Class StreetSheet
        Public Property Id As Guid
        Public Property DateTimeStarted As DateTime
        Public Property DateTimeFinished As DateTime
        Public Property DefaultCity As String
        Public Property Partner As String
        Public Property UserId As Integer
        Public Overridable Property User As User
        Public Overridable Property Houses As ICollection(Of House)

    End Class
End Namespace