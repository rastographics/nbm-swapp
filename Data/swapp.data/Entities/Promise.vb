﻿Namespace Entities
    Public Class Promise
        Public Property Id As Guid
        Public Property DateMade As DateTime
        Public Property PromiseDate As DateTime
        Public Property PromiseType As PromiseType
        Public Property Notes As String
        Public Property ProspectId As Guid
        Public Overridable Property Prospect As Prospect
        Public Property UserId As Integer
        Public Overridable Property User As User
    End Class

    Public Enum PromiseType
        No
        Maybe
        Likely
        Yes
    End Enum
End Namespace