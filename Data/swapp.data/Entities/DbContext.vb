﻿Imports swapp.data.Entities
Imports Microsoft.AspNet.Identity.EntityFramework
Imports System.Data.Entity

Namespace Database
    Public Class DbContext
        Inherits IdentityDbContext(Of User)

        Public Property Contacts As DbSet(Of Contact)
        Public Property Houses As DbSet(Of House)
        Public Property Promises As DbSet(Of Promise)
        Public Property Prospects As DbSet(Of Prospect)
        Public Property Streets As DbSet(Of Street)
        Public Property StreetSheets As DbSet(Of StreetSheet)

        Public Sub New()
            MyBase.New("DefaultConnection", throwIfV1Schema:=False)
        End Sub

        Public Shared Function Create() As DbContext
            Return New DbContext()
        End Function
    End Class
End Namespace
