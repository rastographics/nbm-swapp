﻿Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports System.Threading.Tasks
Imports System.Security.Claims

Namespace Entities
    Public Class User
        Inherits identityUser
        'Inherit identity
        Public Property Name As String
        Public Async Function GenerateUserIdentityAsync(manager As UserManager(Of User), authenticationType As String) As Task(Of ClaimsIdentity)
            ' Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            Dim userIdentity = Await manager.CreateIdentityAsync(Me, authenticationType)
            ' Add custom user claims here
            Return userIdentity
        End Function
    End Class
End Namespace