﻿Namespace Entities
    Public Class Contact
        Public Property Id As Guid
        Public Property UserId As Integer
        Public Property DateTimeOfContact As DateTime
        Public Property Notes As String

    End Class
End Namespace