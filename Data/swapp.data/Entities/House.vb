﻿Namespace Entities
Public Class House
    Public Property Id As Guid
    Public Property StreetId As Guid
    Public Overridable Property Street As Street
    Public Property SheetId As Guid
    Public Overridable Property Sheet As StreetSheet
    Public Property HouseNumber As String
    Public Property ProspectId As Guid?
    Public Overridable Property Prospect As Prospect
    Public Property Contacts As Int16
    Public Property Gospels As Int16
    Public Property Saved As Int16
    Public Property Notes As String
    Public Property DateCreated As DateTime
    Public Property DateKnocked As DateTime?
End Class
End Namespace