﻿Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin
Imports swapp.data.Entities
Imports swapp.data.Database

' Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
Public Class UserManager
    Inherits UserManager(Of User)

    Public Sub New(store As IUserStore(Of User))
        MyBase.New(store)
    End Sub

    Public Shared Function Create(options As IdentityFactoryOptions(Of UserManager), context As IOwinContext)
        Dim manager = New UserManager(New UserStore(Of User)(context.Get(Of DbContext)()))

        ' Configure validation logic for usernames
        manager.UserValidator = New UserValidator(Of User)(manager) With {
            .AllowOnlyAlphanumericUserNames = False,
            .RequireUniqueEmail = True
        }

        ' Configure validation logic for passwords
        manager.PasswordValidator = New PasswordValidator With {
            .RequiredLength = 6,
            .RequireNonLetterOrDigit = True,
            .RequireDigit = True,
            .RequireLowercase = True,
            .RequireUppercase = True
        }

        Dim dataProtectionProvider = options.DataProtectionProvider
        If (dataProtectionProvider IsNot Nothing) Then
            manager.UserTokenProvider = New DataProtectorTokenProvider(Of User)(dataProtectionProvider.Create("ASP.NET Identity"))
        End If

        Return manager
    End Function
End Class
