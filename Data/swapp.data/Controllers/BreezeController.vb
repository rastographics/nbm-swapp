﻿Imports System.Net
Imports System.Web.Http
Imports Breeze.WebApi2
Imports Breeze.ContextProvider

Namespace Controllers
    <BreezeController>
    Public Class BreezeController
        Inherits ApiController
        <HttpGet()>
        Public Function Metadata() As String

        End Function

        <HttpPost()>
        Public Function SaveChanges(JObject, saveBundle) As SaveResult

        End Function
    End Class
End Namespace